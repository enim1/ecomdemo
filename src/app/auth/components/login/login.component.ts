import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';
  error = '';
  loading = false;
  isLoggedIn= false;
  
  constructor(private _auth: AuthService, private _router: Router, private _token: TokenStorageService) {}

  ngOnInit(): void {}

  onSubmit(): void {
    this.loading = true;
    this.error = '';
    if (!this.email || !this.password) {
      this.error = 'Make sure to fill everything ;)';
    } else {
      console.log("dataaallllll")
      this._auth
        .login({ email: this.email, password: this.password })
        .subscribe(
          (res) => {
            console.log("daaaaaaaaaaaaaaaaaaaaatrtr", res)
            this.loading = false;
            let objetRes= JSON.parse(JSON.stringify(res))
            //this.isLoggedIn= true;
            console.log(objetRes);
            console.log(objetRes.token);


            this._token.setToken(objetRes.token+"");
            this._token.setUser("cmt");
            this._router.navigate(['/home']);
          },
          (err) => {
            //console.log(err);
            //console.log("hhhhhhhhhhhhh",err.error.__ERROR[0].message)
            this.error = "Wrong User name or password";
            this.loading = false;
          }
        );
   }
  }
  switchLoginSignUp(){
    this._router.navigate(["register"])
  }
  canSubmit(): boolean {
    return this.email.length > 0 && this.password.length > 0;
  }
}
