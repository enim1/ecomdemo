import {
  Component,
  OnInit,
  ViewEncapsulation,
  HostListener,
} from '@angular/core';
import { CartService } from '../services/cart.service';
import { ProductService } from '../services/product.service';
import { Products, Product } from '../shared/models/product.model';

@Component({
  selector: 'app-home',
  templateUrl: './home-carlos.component.html',
  styleUrls: ['./home-carlos.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeCarlosComponent implements OnInit {
  products: Product[] = [];
  categories: any[] = [
    {
      name: 'Laptops',
    },
    {
      name: 'Phones',
    },
    {
      name: 'Clothes',
    },
  ];
  loading = false;
  productPageCounter = 1;
  additionalLoading = false;

  constructor(
    private productService: ProductService,
    private cartService: CartService
  ) {}

  public screenWidth: any;
  public screenHeight: any;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;
  }

  ngOnInit(): void {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;
    this.loading = true;

  }
}
