import {
  Component,
  OnInit,
  ViewEncapsulation,
  HostListener,
} from '@angular/core';
import { CartService } from '../services/cart.service';
import { ProductService } from '../services/product.service';
import { Products, Product } from '../shared/models/product.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  products: Product[] = [];
  categories: any[] = [
    {
      name: 'Laptops',
    },
    {
      name: 'Phones',
    },
    {
      name: 'Clothes',
    },
  ];
  loading = false;
  productPageCounter = 1;
  additionalLoading = false;

  constructor(
    private productService: ProductService,
    private cartService: CartService
  ) {}

  public screenWidth: any;
  public screenHeight: any;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;
  }
  filterSortObject={
    category:{     
       sortFn: (a: any, b: any) => a.category<b.category,
       filterFn:(list: string[], item: any) => list.some(name => item.category.indexOf(name) !== -1),
       sortDirections: ['ascend', 'descend', null],
       sortOrder: null,

      },
    title:"",
    description:"",
    price:"",
    rating:"",
    stock:"",
  }
  ngOnInit(): void {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;
    this.loading = true;
    setTimeout(() => {
      this.productService.getAllProducts().subscribe(
        (res: any) => {
          console.log(res);
          this.products = res.data;
          this.loading = false;
        },
        (err) => {
          console.log(err);
          this.loading = false;
        }
      );
    }, 500);
  }
}
