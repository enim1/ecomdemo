import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import { map } from 'rxjs/operators';

// import Swiper core and required components
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller,
} from 'swiper/core';
import { CartService } from '../services/cart.service';

// install Swiper components
SwiperCore.use([
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller,
]);

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  id: number;
  product: any;
  quantity: number;
  showcaseImages: any[] = [];
  loading = false;

  constructor(
    private _route: Router,
    private _product: ProductService,
    private _cart: CartService,
  ) {}

  ngOnInit(): void {
    if(window.history.state.price){
      console.log(window.history.state);
      
      this.product=window.history.state;
    }
    else{
      this._route.navigate(["/product"]);
    }
    this.loading = true;
  }
  alertSuccess(){
    if(this.quantity){
    alert(this.product.title + ' Successfully buyed '+ this.quantity +' item(s)')
  }
  else{
    alert("select quantity of product")
  }
  } 
}
