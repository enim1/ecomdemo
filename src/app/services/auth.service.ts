import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from './api.service';
import { TokenStorageService } from './token-storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private userSubject: BehaviorSubject<any>;
  public user: Observable<any>;
  private baseUrl = environment.apiUrl;
  constructor(private _api: ApiService, private _token: TokenStorageService, private _httpS: HttpClient) {
    this.userSubject = new BehaviorSubject<any>(this._token.getUser());
    this.user = this.userSubject.asObservable();
  }

  getUser() {
    console.log(this.userSubject);
    console.log(this.userSubject.value);
    return this.userSubject.value;
  }

  register(credentials: any): Observable<any> {
    const headers= new HttpHeaders()
    .set('username-4D', credentials.userName)
    .set('password-4D', credentials.password)
    .set('Access-Control-Allow-Origin', '*');
   let  payload: object=credentials;
   let url :string="/4DACTION/register"
return this._httpS.post(`${this.baseUrl}${url}`,JSON.stringify(payload), {'headers':headers})
    //return this._api.postTypeRequest('$directory/login', JSON.stringify(payload));
  }

  login(credentials: any): Observable<any> {
    const headers= new HttpHeaders()
    .set('username-4D', credentials.email)
    .set('password-4D', credentials.password)
    .set('Access-Control-Allow-Origin', '*');
   let  payload: object=credentials;
   let url :string="/4DACTION/login"
return this._httpS.post(`${this.baseUrl}${url}`,JSON.stringify(payload), {'headers':headers})
    //return this._api.postTypeRequest('$directory/login', JSON.stringify(payload));

  }

  logout(Token:any):void {
    this._token.clearStorage();
    const headers= new HttpHeaders()

    .set('Token',Token)
    .set('Access-Control-Allow-Origin', '*');
    let url :string="/4DACTION/logout"
    this._httpS.post(`${this.baseUrl}${url}`,"",{'headers':headers} ).subscribe((res)=>{
      console.log(res)
    })
    this.userSubject.next(null);
  }
}
