import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Products, Product } from '../shared/models/product.model';
import { environment } from '../../environments/environment';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private url = environment.apiUrl+"/4DACTION";

  constructor(private http: HttpClient, private _api: ApiService) {}

  getAllProducts(): Observable<any> {
    
    return this.http.get(this.url + '/AllProducts'); 
  }

  getSingleProduct(id: Number): Observable<any> {
    /* console.log(id);*/
    return /*this._api.getTypeRequest('products/' + id); */
  }
}
